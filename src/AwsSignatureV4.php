<?php
namespace Leaddesk\Services;

class AwsSignatureV4
{
    private $service;
    private $region;

    const REQUEST_TYPE = "aws4_request";
    
    public function __construct($service, $region) 
    {
        $this->service = $service;
        $this->region = $region;
    }
    
    public function signRequest(array $request, $awsKey, $awsSecret)
    {
        $longDate = gmdate("Ymd\THis\Z");
        $shortDate = gmdate("Ymd");

        // Step 1: Generate the Scope
        $scope = array(
            $shortDate,
            $this->region,
            $this->service,
            self::REQUEST_TYPE
        );
        $credentialScope = implode('/', $scope);

        // Step 2: create signature string for canonical request
        $payload = strtolower(hash('sha256', $request['body']));
        $request['headers']['X-Amz-Date'] = array($longDate);
        $context = $this->createContext($request, $payload);
        $hash = hash('sha256', $context['creq']);
        $signatureString = "AWS4-HMAC-SHA256\n{$longDate}\n{$credentialScope}\n{$hash}";

        // Step 3: Signing your Request (Making a Signature)
        $dateKey = hash_hmac('sha256', $shortDate, 'AWS4' . $awsSecret, true);
        $dateRegionKey = hash_hmac('sha256', $this->region, $dateKey, true);
        $dateRegionServiceKey = hash_hmac('sha256', $this->service, $dateRegionKey, true);
        $signingKey = hash_hmac('sha256', self::REQUEST_TYPE, $dateRegionServiceKey, true);

        $signature = hash_hmac('sha256', $signatureString, $signingKey);


        $parsed['headers']['Authorization'] = 
            "AWS4-HMAC-SHA256 "
            . "Credential={$awsKey}/{$credentialScope}, "
            . "SignedHeaders={$context['headers']}, Signature={$signature}";
        $parsed['headers']['X-Amz-Date'] = $longDate;
      
        return $parsed;
    }
    
    private function createContext(array $parsedRequest, $payload)
    {
        // The following headers are not signed because signing these headers
        // would potentially cause a signature mismatch when sending a request
        // through a proxy or if modified at the HTTP client level.
        static $blacklist = array(
            'cache-control'       => true,
            'content-type'        => true,
            'content-length'      => true,
            'expect'              => true,
            'max-forwards'        => true,
            'pragma'              => true,
            'range'               => true,
            'te'                  => true,
            'if-match'            => true,
            'if-none-match'       => true,
            'if-modified-since'   => true,
            'if-unmodified-since' => true,
            'if-range'            => true,
            'accept'              => true,
            'authorization'       => true,
            'proxy-authorization' => true,
            'from'                => true,
            'referer'             => true,
            'user-agent'          => true
        );

        // Normalize the path as required by SigV4
        $canon = $parsedRequest['method'] . "\n"
            . $this->createCanonicalizedPath($parsedRequest['path']) . "\n"
            . $this->getCanonicalizedQuery($parsedRequest['query']) . "\n";

        // Case-insensitively aggregate all of the headers.
        $aggregate = array();
        foreach ($parsedRequest['headers'] as $key => $values) {
            $key = strtolower($key);
            if (!isset($blacklist[$key])) {
                foreach ($values as $v) {
                    $aggregate[$key][] = $v;
                }
            }
        }

        ksort($aggregate);
        $canonHeaders = array();
        foreach ($aggregate as $k => $v) {
            if (count($v) > 0) {
                sort($v);
            }
            $canonHeaders[] = $k . ':' . preg_replace('/\s+/', ' ', implode(',', $v));
        }

        $signedHeadersString = implode(';', array_keys($aggregate));
        $canon .= implode("\n", $canonHeaders) . "\n\n"
            . $signedHeadersString . "\n"
            . $payload;

        return array('creq' => $canon, 'headers' => $signedHeadersString);
    }
    
    protected function createCanonicalizedPath($path)
    {
        $doubleEncoded = rawurlencode(ltrim($path, '/'));
        return '/' . str_replace('%2F', '/', $doubleEncoded);
    }
    
    private function getCanonicalizedQuery(array $query)
    {
        unset($query['X-Amz-Signature']);

        if (!$query) {
            return '';
        }

        $qs = '';
        ksort($query);
        foreach ($query as $k => $v) {
            if (!is_array($v)) {
                $qs .= rawurlencode($k) . '=' . rawurlencode($v) . '&';
            } else {
                sort($v);
                foreach ($v as $value) {
                    $qs .= rawurlencode($k) . '=' . rawurlencode($value) . '&';
                }
            }
        }

        return substr($qs, 0, -1);
    }    
}