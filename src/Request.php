<?php
/**
 * Call Leaddesk services through AWS API gateway
 * Works with PHP 5.3+
 *
 */
namespace Leaddesk\Services;

require 'AwsSignatureV4.php';

class Request
{
    private $awsKey;
    private $awsSecret;
    private $resource;
    private $method;
    private $headers;
    private $body;
    private $headerNames;
    private $awsRegion = 'eu-central-1';
    private $apiGatewayUrl = 'https://lbqhwdmmkf.execute-api.eu-central-1.amazonaws.com/stage';

    const SERVICE = 'execute-api';


    /**
     * Creates request to Leaddesk services API gateway with given IAM user key
     *
     * @param string $awsKey - IAM user key
     * @param string $awsSecret - IAM user secret
     */
    public function __construct($awsKey, $awsSecret)
    {
        $this->awsKey = $awsKey;
        $this->awsSecret = $awsSecret;
        $this->headers = array();
    }

    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    public function setMethod($method)
    {
        $this->method = strtoupper($method);
    }

    public function setHeaders(array $headers)
    {
        $this->headerNames = $this->headers = array();
        foreach ($headers as $header => $value) {
            $this->addHeader($header, $value);
        }
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function setAwsRegion($region)
    {
        $this->awsRegion = $region;
    }

    public function setApiGatewayUrl($url)
    {
        $this->apiGatewayUrl = $url;
    }

    /**
     * Makes call to service resource
     *
     * returns reponse as array
     *
     * @return array - response array
     */
    public function call()
    {
        $url = $this->apiGatewayUrl . $this->resource;

        // Insert Host header if not found
        if (!$this->hasHeader('Host')) {
            $parts = parse_url($this->apiGatewayUrl);
            $hostHeader = array('Host' => array($parts['host']));
            $this->headers = $hostHeader + $this->headers;
        }

        $signed = $this->signRequest($url);
        $this->addHeader('Authorization', $signed['headers']['Authorization']);
        $this->addHeader('X-amz-date', $signed['headers']['X-Amz-Date']);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getCurlHeaders());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($this->method != 'GET') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->body);
        }

        // SET TIMEOUTS
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function hasHeader($header)
    {
        return isset($this->headerNames[strtolower($header)]);
    }

    public function addHeader($header, $value)
    {
        if (!is_array($value)) {
            $value = array($value);
        }

        $value = $this->trimHeaderValues($value);
        $normalized = strtolower($header);
        if ($this->hasHeader($header)) {
            $header = $this->headerNames[$normalized];
            $this->headers[$header] = array_merge($this->headers[$header], $value);
        } else {
            $this->headerNames[$normalized] = $header;
            $this->headers[$header] = $value;
        }
    }

    private function getCurlHeaders()
    {
        $headers = array();
        foreach ($this->headers as $header => $values) {
            foreach ($values as $value) {
                $headers[] = "{$header}: {$value}";
            }
        }
        return $headers;
    }

    private function signRequest()
    {
        $aws = new AwsSignatureV4(self::SERVICE, $this->awsRegion);
        return $aws->signRequest($this->createRequestArray(), $this->awsKey, $this->awsSecret);
    }

    private function trimHeaderValues(array $values)
    {
        return array_map(function ($value) {
            return trim($value, " \t");
        }, $values);
    }

    private function createRequestArray()
    {
        $urlParts = parse_url($this->apiGatewayUrl . $this->resource);
        $urlParts['query'] = isset($urlParts['query']) ? $urlParts['query'] : '';

        $request = array(
            'method'  => $this->method,
            'path'    => $urlParts['path'],
            'query'   => $this->parseQuery($urlParts['query']),
            'uri'     => $urlParts['scheme']  . '://' . $urlParts['host'],
            'headers' => $this->headers,
            'body'    => ($this->method == 'GET') ? '' : $this->body
        );
        return $request;
    }

    private function parseQuery($str, $urlEncoding = true)
    {
        $result = array();

        if ($str === '') {
            return $result;
        }

        if ($urlEncoding === true) {
            $decoder = function ($value) {
                return rawurldecode(str_replace('+', ' ', $value));
            };
        } elseif ($urlEncoding == PHP_QUERY_RFC3986) {
            $decoder = 'rawurldecode';
        } elseif ($urlEncoding == PHP_QUERY_RFC1738) {
            $decoder = 'urldecode';
        } else {
            $decoder = function ($str) { return $str; };
        }

        foreach (explode('&', $str) as $kvp) {
            $parts = explode('=', $kvp, 2);
            $key = $decoder($parts[0]);
            $value = isset($parts[1]) ? $decoder($parts[1]) : null;
            if (!isset($result[$key])) {
                $result[$key] = $value;
            } else {
                if (!is_array($result[$key])) {
                    $result[$key] = array($result[$key]);
                }
                $result[$key][] = $value;
            }
        }

        return $result;
    }
}