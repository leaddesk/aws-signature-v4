<?php
/*
To use this code create composer.json with following content:
 
{
    "require": {
        "aws/aws-sdk-php": "~3.19"
    }
}

execute in console:
composer install


*/

require 'vendor/autoload.php';

use Aws\Signature\SignatureV4;
use Aws\Credentials\CredentialProvider;
use Aws\Credentials\Credentials;


$url = 'https://lbqhwdmmkf.execute-api.eu-central-1.amazonaws.com/stage/scheduler/tasks/create';

$payload = '{'
   . '"owner": "Client 4",'
   . '"url": "http://api.leaddesk.com",'
   . '"timeSlice": "1minute",'
   . '"tag": "SMSForFriend",'
   . '"count": "1",'
   . '"retries": "1",'
   . '"payload": "auth=3e9a57c3aa7bf1d1f7825a8148169fdc&mod=messaging&cmd=send_sms&sender=Steve&receiver=0445939433&message=This is message for you my friend!",'
   . '"contentType": "text/plain"'
   . '}';

$request = new \GuzzleHttp\Psr7\Request(
    'POST', 
    $url, 
    ['Content-Type' => 'application/json'], 
    $payload
);

$signature = new SignatureV4('execute-api', 'eu-central-1');
$provider = CredentialProvider::defaultProvider();
$credentials = $provider()->wait();
$signedRequest = $signature->signRequest($request, $credentials);

// send requesst
$requ = new \GuzzleHttp\Client([
    'timeout' => '5'
]);


$resp = $requ->send($signedRequest);

echo $resp->getBody();