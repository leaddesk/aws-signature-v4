<?php
require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use Leaddesk\Services\Request;

/* GET */

$getRequest = new Request('YOUR_IAM_KEY', 'YOUR_IAM_SECRET');
$getRequest->setResource('/scheduler/tasks');
$getRequest->setMethod('GET');
$getRequest->setHeaders(array(
    'Content-Type' => 'text/plain'
));
echo $getRequest->call();


/* POST */

$request = new Request('YOUR_IAM_KEY', 'YOUR_IAM_SECRET');
$request->setResource('/scheduler/tasks/create');
$request->setMethod('POST');
$request->setHeaders(array(
    'Content-Type' => 'application/json'
));

$ldAuth = 'GIVE VALID LEADDESK CLIENT IDENTIFICATION HASH';
$receiverPhone = 'YOUR PHONE NUMBER';
$payload = '{'
   . '"owner": "Client 4",'
   . '"url": "http://api.leaddesk.com",'
   . '"timeSlice": "1minute",'
   . '"tag": "SMSForFriend2",'
   . '"count": "1",'
   . '"retries": "1",'
   . '"payload": "auth=' . $ldAuth . '&mod=messaging&cmd=send_sms&sender=Steve&receiver=' . $receiverPhone . '&message=This is message for you my friend!",'
   . '"contentType": "text/plain"'
   . '}';

$request->setBody($payload);

echo $request->call();